import numpy as np
import netCDF4 as nc4
from scipy.io import loadmat
import matplotlib.pyplot as plt
import torch
import matplotlib.image as mpimg
import seaborn as sns
import bottleneck as bn
from scipy import signal
import scipy
b, a = signal.butter(20, 1/5,btype='lowpass')

data_dir = '../data_pre_ind_2/'


cluster_map = np.zeros((36,72),dtype=int)
cluster_map[0:18,:] = 1





fn = data_dir+'obs.nc'

f = nc4.Dataset(fn, 'r')

data = f.variables['temperature_anomaly'][:]
LAT = f.variables['latitude'][:]




#fonction faisant la moyenne spatiale d'une simulation
def get_mean(data,cluster=-1):
    t = np.zeros((data.shape[0]))
    if(cluster==-1):
        div = 0
        for j in range(36):
            for k in range(72):
                t += data[:, j, k] * np.cos(np.radians(LAT[j]))

                div += np.cos(np.radians(LAT[j]))
        t /= div
        return t
    else:
        div = 0
        for j in range(36):
            for k in range(72):
                if(cluster_map[j,k]==cluster):
                    t += data[:, j, k] * np.cos(np.radians(LAT[j]))
                    div += np.cos(np.radians(LAT[j]))

        t /= div

        return t

#fonction extrayant les observations
def get_obs(cluster=-1):
    fn = data_dir + 'obs.nc'

    f = nc4.Dataset(fn, 'r')
    data = f.variables['temperature_anomaly'][:]

    return get_mean(data,cluster=cluster)








# fonction renvoyant 1 simulation
def get_simu(type,simu,model='IPSL',cluster=-1,filtrage=False,phys=1):
    
    if(type=='historical'):
        fn = data_dir + model + '_' + type + '_' + str(simu) + '.nc'
        f = nc4.Dataset(fn, 'r')
        pre_ind = np.mean(get_mean(f.variables['tas'][0:50],cluster=cluster))
        data = get_mean(f.variables['tas'][50:],cluster=cluster)
        result = data - pre_ind
    
    else:
        
        if(model=='GISS'):
            i = simu
            if(i!=5 and i!=6 and i!=7 and i!=8 and i!=9):
                phys='1'
                pre_ind = np.mean(get_mean((np.load(data_dir+'pi_contr_'+model+'_phys_'+phys+'.npy')),cluster=cluster))
            else:
                phys='3'
                pre_ind = np.mean(get_mean((np.load(data_dir+'pi_contr_'+model+'_phys_'+phys+'.npy')),cluster=cluster))
        else:
            pre_ind = np.mean(get_mean((np.load(data_dir+'pi_contr_'+model+'.npy')),cluster=cluster))


        fn = data_dir + model + '_' + type + '_' + str(simu) + '.nc'
        f = nc4.Dataset(fn, 'r')
        data = get_mean(f.variables['tas'][50:],cluster=cluster)
        result = data - pre_ind
    if(filtrage):
        if(type=='hist-GHG' or type=='hist-aer'):
            #result = bn.move_mean(result, window=5, min_count=1)
            result = signal.filtfilt(b, a, result)
    return result

#


#fonction renvoyant les simulations d'un certain type d'un modèle climatique
def get_data_forcage(type,model='IPSL',cluster=-1,filtrage=False):
    if(model=='IPSL'):

        dic = {'hist-GHG' : 10,'hist-aer' : 10, 'hist-nat' : 10,'historical' : 32}
        result = np.zeros((dic[type],115))
        for i in range(dic[type]):
            result[i] = get_simu(type,i+1,model,cluster,filtrage=filtrage)[0:115]

    if(model=='CNRM'):

        dic = {'hist-GHG' : 9,'hist-aer' : 10, 'hist-nat' : 10,'historical' : 30}
        result = np.zeros((dic[type],115))
        for i in range(dic[type]):
            result[i] = get_simu(type,i+1,model,cluster,filtrage=filtrage)[0:115]

    if(model=='CESM2'):

        dic = {'hist-GHG' : 3,'hist-aer' : 2, 'hist-nat' : 3,'historical' : 11}
        result = np.zeros((dic[type],115))
        for i in range(dic[type]):
            result[i] = get_simu(type,i+1,model,cluster,filtrage=filtrage)[0:115]


    if (model == 'ACCESS'):

        dic = {'hist-GHG': 3, 'hist-aer': 3, 'hist-nat': 3, 'historical': 30}
        result = np.zeros((dic[type], 115))
        for i in range(dic[type]):
            result[i] = get_simu(type, i + 1, model,cluster,filtrage=filtrage)[0:115]

    if (model == 'BCC'):

        dic = {'hist-GHG': 3, 'hist-aer': 3, 'hist-nat': 3, 'historical': 3}
        result = np.zeros((dic[type], 115))
        for i in range(dic[type]):
            result[i] = get_simu(type, i + 1, model,cluster,filtrage=filtrage)[0:115]

    if (model == 'CanESM5'):

        dic = {'hist-GHG': 50, 'hist-aer': 30, 'hist-nat': 50, 'historical': 65}
        result = np.zeros((dic[type], 115))
        for i in range(dic[type]):
            result[i] = get_simu(type, i + 1, model,cluster,filtrage=filtrage)[0:115]

    if (model == 'FGOALS'):

        dic = {'hist-GHG': 3, 'hist-aer': 3, 'hist-nat': 3, 'historical': 6}
        result = np.zeros((dic[type], 115))
        for i in range(dic[type]):
            result[i] = get_simu(type, i + 1, model,cluster,filtrage=filtrage)[0:115]

    if (model == 'GISS'):

        dic = {'hist-GHG': 10, 'hist-aer': 12, 'hist-nat': 20, 'historical': 19}
        result = np.zeros((dic[type], 115))
        for i in range(dic[type]):
            result[i] = get_simu(type, i + 1, model,cluster,filtrage=filtrage)[0:115]

    if (model == 'HadGEM3'):

        dic = {'hist-GHG': 4, 'hist-aer': 4, 'hist-nat': 4, 'historical': 5}
        result = np.zeros((dic[type], 115))
        for i in range(dic[type]):
            result[i] = get_simu(type, i + 1, model,cluster,filtrage=filtrage)[0:115]

    if (model == 'MIRO'):

        dic = {'hist-GHG': 3, 'hist-aer': 3, 'hist-nat': 3, 'historical': 50}
        result = np.zeros((dic[type], 115))
        for i in range(dic[type]):
            result[i] = get_simu(type, i + 1, model,cluster,filtrage=filtrage)[0:115]

    if (model == 'ESM2'):

        dic = {'hist-GHG': 5, 'hist-aer': 5, 'hist-nat': 5, 'historical': 5}
        result = np.zeros((dic[type], 115))
        for i in range(dic[type]):
            result[i] = get_simu(type, i + 1, model,cluster,filtrage=filtrage)[0:115]

    if (model == 'NorESM2'):

        dic = {'hist-GHG': 3, 'hist-aer': 3, 'hist-nat': 3, 'historical': 3}
        result = np.zeros((dic[type], 115))
        for i in range(dic[type]):
            result[i] = get_simu(type, i + 1, model,cluster,filtrage=filtrage)[0:115]


    return(result)

# fonction renvoyant le data-set entier traité
def get_data_set(model='IPSL',cluster=-1,normalis=False,filtrage=False):
    liste_max = []
    if (model != 'ALL'):

        aer = get_data_forcage('hist-aer', model=model,cluster=cluster,filtrage=filtrage)[:,0:115]
        ghg = get_data_forcage('hist-GHG', model=model,cluster=cluster,filtrage=filtrage)[:,0:115]
        nat = get_data_forcage('hist-nat', model=model,cluster=cluster,filtrage=filtrage)[:,0:115]
        historical = get_data_forcage('historical', model=model,cluster=cluster,filtrage=filtrage)[:,0:115]
        max_hist = np.max(np.mean(historical, axis=0))
        liste_max.append(max_hist)
        if(normalis):


            aer = aer /max_hist
            ghg = ghg / max_hist
            nat = nat / max_hist
            historical = historical/ max_hist



    elif (model == 'ALL'):


        #liste_models = ['CanESM5', 'CNRM', 'GISS', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
        #                'NorESM2','CESM2']
        liste_models = ['CanESM5', 'CNRM', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
                        'NorESM2','CESM2','GISS']

        aer = []
        ghg = []
        nat = []
        historical = []

        for model_curr in liste_models:
            print(model_curr)

            aer_curr = torch.tensor(get_data_forcage('hist-aer', model=model_curr,cluster=cluster,filtrage=filtrage)[:, 0:115])
            ghg_curr = torch.tensor(get_data_forcage('hist-GHG', model=model_curr,cluster=cluster,filtrage=filtrage)[:, 0:115])
            nat_curr = torch.tensor(get_data_forcage('hist-nat', model=model_curr,cluster=cluster,filtrage=filtrage)[:, 0:115])
            historical_curr = torch.tensor(get_data_forcage('historical', model=model_curr,cluster=cluster,filtrage=filtrage)[:, 0:115])
            max_hist = torch.max(torch.mean(historical_curr, dim=0))
            liste_max.append(max_hist)

            if (normalis):

                aer_curr = aer_curr / max_hist
                ghg_curr = ghg_curr / max_hist
                nat_curr = nat_curr / max_hist
                historical_curr = historical_curr / max_hist



            aer.append(aer_curr)
            ghg.append(ghg_curr)
            nat.append(nat_curr)
            historical.append(historical_curr)

        return ghg, aer, nat, historical,np.array(liste_max)

    return torch.tensor(ghg).float(), torch.tensor(aer).float(), torch.tensor(nat).float(), torch.tensor(
        historical).float(),np.array(liste_max)



def get_std_global(model,normalis=False,cluster=-1):
        aer = get_data_forcage('hist-aer',model=model,cluster=cluster)
        ghg = get_data_forcage('hist-GHG',model=model,cluster=cluster)
        nat = get_data_forcage('hist-nat',model=model,cluster=cluster)
        historical = get_data_forcage('historical',model=model,cluster=cluster)
        
        aer = (aer - np.mean(aer,axis=0))**2
        
        
        ghg = (ghg - np.mean(ghg,axis=0))**2
        nat = (nat - np.mean(nat,axis=0))**2
        historical = (historical - np.mean(historical,axis=0))**2
        
        var_aer = np.sum(aer) * (1/(aer.shape[0]*aer.shape[1]-1))
        var_ghg = np.sum(ghg) * (1/(ghg.shape[0]*ghg.shape[1]-1))
        var_nat = np.sum(nat) * (1/(nat.shape[0]*nat.shape[1]-1))
        var_hist = np.sum(historical) * (1/(historical.shape[0]*historical.shape[1]-1))
        
        var_mod = aer.shape[0] * var_aer + ghg.shape[0] * var_ghg + nat.shape[0] * var_nat + historical.shape[0] * var_hist
        var_mod /=(aer.shape[0]+nat.shape[0]+ghg.shape[0]+historical.shape[0]) 
        var_mod = np.sqrt(var_mod)
        return var_mod
    



        
  


#renvoie les simulations moyenne de modèle climtique
def get_mean_data_set(model='IPSL',normalis=False,cluster=-1,filtrage=False):
    if(model!='ALL'):

        aer = np.mean(get_data_forcage('hist-aer',model=model,cluster=cluster,filtrage=filtrage),axis=0)
        ghg = np.mean(get_data_forcage('hist-GHG',model=model,cluster=cluster,filtrage=filtrage),axis=0)
        nat = np.mean(get_data_forcage('hist-nat',model=model,cluster=cluster,filtrage=filtrage),axis=0)
        historical = np.mean(get_data_forcage('historical',model=model,cluster=cluster,filtrage=filtrage),axis=0)

        if(normalis):

            max_hist = np.max(historical)
            aer = aer /max_hist
            ghg = ghg / max_hist
            nat = nat / max_hist
            historical = historical/ max_hist



    elif(model=='ALL'):


        # liste_models = ['CanESM5', 'CNRM', 'GISS', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
        #                 'NorESM2','CESM2']
        liste_models = ['CanESM5', 'CNRM', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
                        'NorESM2','CESM2','GISS']
        result = []
        historical = []
        for model_curr in liste_models :

            aer_ipsl = np.mean(get_data_forcage('hist-aer',model=model_curr,cluster=cluster,filtrage=filtrage),axis=0)
            ghg_ipsl = np.mean(get_data_forcage('hist-GHG',model=model_curr,cluster=cluster,filtrage=filtrage),axis=0)
            nat_ipsl = np.mean(get_data_forcage('hist-nat',model=model_curr,cluster=cluster,filtrage=filtrage),axis=0)
            historical_ipsl = np.mean(get_data_forcage('historical',model=model_curr,cluster=cluster,filtrage=filtrage),axis=0)

            if (normalis):

                max_hist = np.max(historical_ipsl)


                aer_ipsl = aer_ipsl / max_hist
                ghg_ipsl = ghg_ipsl / max_hist
                nat_ipsl = nat_ipsl / max_hist
                historical_ipsl = historical_ipsl / max_hist


            result_ipsl = np.stack((ghg_ipsl, aer_ipsl, nat_ipsl))
            result.append(result_ipsl)
            historical.append(historical_ipsl)

        result = np.array(result)
        result = np.mean(result,axis=0)

        historical = np.array(historical)
        historical = np.mean(historical,axis=0)



        return torch.tensor(result).unsqueeze(0), historical

    result = np.stack((ghg, aer, nat))
    return torch.tensor(result).unsqueeze(0), historical





def get_mean_CI(cluster=-1,filtrage=False):



        # liste_models = ['CanESM5', 'CNRM', 'GISS', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
        #                 'NorESM2','CESM2']
    liste_models = ['CanESM5', 'CNRM', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
                    'NorESM2','CESM2','GISS']

    GHG = []
    AER = []
    NAT = []
    for model_curr in liste_models :

        data = get_data_forcage('hist-aer',model=model_curr,cluster=cluster,filtrage=filtrage)
        N = data.shape[0]
        t = scipy.stats.t.ppf(q=1-0.05/2,df=N-1)
        mean = np.mean(data,axis=0)
        std = np.std(data,axis=0)
        resu = np.stack((mean - (t*std)/np.sqrt(N),mean, mean + (t*std)/np.sqrt(N)))
        AER.append(resu)
        
        data = get_data_forcage('hist-GHG',model=model_curr,cluster=cluster,filtrage=filtrage)
        N = data.shape[0]
        t = scipy.stats.t.ppf(q=1-0.05/2,df=N-1)
        mean = np.mean(data,axis=0)
        std = np.std(data,axis=0)
        resu = np.stack((mean - (t*std)/np.sqrt(N),mean, mean + (t*std)/np.sqrt(N)))
        GHG.append(resu)
        
        data = get_data_forcage('hist-nat',model=model_curr,cluster=cluster,filtrage=filtrage)
        N = data.shape[0]
        t = scipy.stats.t.ppf(q=1-0.05/2,df=N-1)
        mean = np.mean(data,axis=0)
        std = np.std(data,axis=0)
        resu = np.stack((mean - (t*std)/np.sqrt(N),mean, mean + (t*std)/np.sqrt(N)))
        NAT.append(resu)
        
    result = np.stack((GHG,AER,NAT))


    return result





#fonction renvoyant l"'écart moyen d'un modèle climatique
def get_std_data_set(model='IPSL',cluster=-1,normalis=False,filtrage=False):
    if(model!='ALL'):

        aer = get_data_forcage('hist-aer', model=model,cluster=cluster,filtrage=filtrage)[:,0:115]
        ghg = get_data_forcage('hist-GHG', model=model,cluster=cluster,filtrage=filtrage)[:,0:115]
        nat = get_data_forcage('hist-nat', model=model,cluster=cluster,filtrage=filtrage)[:,0:115]
        historical = get_data_forcage('historical', model=model,cluster=cluster,filtrage=filtrage)[:,0:115]
        if(normalis):

            max_hist = np.max(np.mean(historical,axis=0))
            aer = aer /max_hist
            ghg = ghg / max_hist
            nat = nat / max_hist
            historical = historical/ max_hist

        aer = np.std(aer,axis=0)
        ghg = np.std(ghg,axis=0)
        nat = np.std(nat,axis=0)
        historical = np.std(historical,axis=0)



    elif(model=='ALL'):


        # liste_models = ['CanESM5', 'CNRM', 'GISS', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
        #                 'NorESM2','CESM2']
        liste_models = ['CanESM5', 'CNRM', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
                        'NorESM2','CESM2','GISS']
        result = []
        historical = []
        for model_curr in liste_models :

            aer_ipsl = get_data_forcage('hist-aer', model=model_curr, cluster=cluster,filtrage=filtrage)[:, 0:115]
            ghg_ipsl = get_data_forcage('hist-GHG', model=model_curr, cluster=cluster,filtrage=filtrage)[:, 0:115]
            nat_ipsl = get_data_forcage('hist-nat', model=model_curr, cluster=cluster,filtrage=filtrage)[:, 0:115]
            historical_ipsl = get_data_forcage('historical', model=model_curr, cluster=cluster,filtrage=filtrage)[:, 0:115]
            if (normalis):
                max_hist = np.max(np.mean(historical_ipsl, axis=0))
                aer_ipsl = aer_ipsl / max_hist
                ghg_ipsl = ghg_ipsl / max_hist
                nat_ipsl = nat_ipsl / max_hist
                historical_ipsl = historical_ipsl / max_hist

            aer_ipsl = np.std(aer_ipsl, axis=0)
            ghg_ipsl = np.std(ghg_ipsl, axis=0)
            nat_ipsl = np.std(nat_ipsl, axis=0)
            historical_ipsl = np.std(historical_ipsl, axis=0)




            result_ipsl = np.stack((ghg_ipsl, aer_ipsl, nat_ipsl))
            result.append(result_ipsl)
            historical.append(historical_ipsl)

        result = np.array(result)
        result = np.mean(result,axis=0)

        historical = np.array(historical)
        historical = np.mean(historical,axis=0)



        return torch.tensor(result).unsqueeze(0), historical

    result = np.stack((ghg, aer, nat))
    return torch.tensor(result).unsqueeze(0), historical


def fig_std():

    liste_models = ['CanESM5', 'CNRM', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2', 'NorESM2',
                    'CESM2', 'GISS', 'ALL']
    model_true_name = ['CanESM5', 'CNRM-CM6-1', 'IPSL-CM6A-LR', 'ACCESS-ESM1-5',
                       'BCC-CSM2-MR', 'FGOALS-g3', 'HadGEM3', 'MIROC6', 'MRI-ESM2.0', 'NorESM2-LM', 'CESM2', 'GISS-E2-1-G', 'ALL']


    fig, axs = plt.subplots(3,4,figsize=([16,12]),gridspec_kw={'width_ratios': [2,2,2,2]})
    ecart = []
    for model in range(12):


        data_true_inv, inver_cible = get_mean_data_set(liste_models[model], cluster=-1, normalis=False,
                                                            filtrage=False)

        std_true_inv, useless_std = get_std_data_set(liste_models[model], cluster=-1, normalis=False, filtrage=False)
        print(std_true_inv)



        i = model // 4
        j = model % 4


        axs[i,j].plot(data_true_inv[0, 0], 'purple', label='GHG')
        axs[i,j].fill_between(np.arange(115), data_true_inv[0, 0] -  std_true_inv[0, 0],
                            data_true_inv[0, 0] + std_true_inv[0, 0],
                            facecolor='purple', alpha=0.2)
        axs[i,j].plot(data_true_inv[0, 1], 'darkblue', label='AER')
        axs[i,j].fill_between(np.arange(115), data_true_inv[0, 1] - std_true_inv[0, 1],
                            data_true_inv[0, 1] +  std_true_inv[0, 1],
                            facecolor='darkblue', alpha=0.2)
        axs[i,j].plot(data_true_inv[0, 2], 'olive', label='NAT')
        axs[i,j].fill_between(np.arange(115), data_true_inv[0, 2] - std_true_inv[0, 2],
                            data_true_inv[0, 2] + std_true_inv[0, 2],
                            facecolor='olive', alpha=0.2)



        axs[i,j].set_ylim((-1.4, 2.2))
        axs[i,j].set_title(liste_models[model])



        if(j!=0):
            axs[i,j].set_yticklabels([])
        else:
            axs[i,j].set_ylabel('°C')
        if(i!=2):
            axs[i,j].set_xticklabels([])
        else:
            axs[i,j].set_xlabel('Years')
            axs[i,j].set_xticks([0, 20,  40,  60,  80,  100, 114])
            axs[i,j].set_xticklabels(
                ['1900',  '1920',  '1940',  '1960',  '1980', '2000', '2014'])
        axs[0,0].legend(frameon=False)
        



    plt.savefig('figures/std_data')
    plt.show()
    
    
def fig_CI():

    liste_models = ['CanESM5', 'CNRM', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2', 'NorESM2',
                    'CESM2', 'GISS', 'ALL']
    model_true_name = ['CanESM5', 'CNRM-CM6-1', 'IPSL-CM6A-LR', 'ACCESS-ESM1-5',
                       'BCC-CSM2-MR', 'FGOALS-g3', 'HadGEM3', 'MIROC6', 'MRI-ESM2.0', 'NorESM2-LM', 'CESM2', 'GISS-E2-1-G', 'ALL']


    fig, axs = plt.subplots(3,4,figsize=([16,12]),gridspec_kw={'width_ratios': [2,2,2,2]})
    data = get_mean_CI()
    ecart = []
    for index in range(12):






        i = index // 4
        j = index % 4


        axs[i,j].plot(data[0, index,1], 'purple', label='True GHG')
        axs[i,j].fill_between(np.arange(115),data[0, index,0], data[0, index,2], facecolor='purple', alpha=0.2)
        axs[i,j].plot(data[1, index,1], 'darkblue', label='True AER')
        axs[i,j].fill_between(np.arange(115),data[1,index,0], data[1, index,2], facecolor='darkblue', alpha=0.2)
        axs[i,j].plot(data[2, index,1], 'olive', label='True NAT')
        axs[i,j].fill_between(np.arange(115),data[2, index,0], data[2, index,2], facecolor='olive', alpha=0.2)



        axs[i,j].set_ylim((-1.4, 2.2))
        axs[i,j].set_title(liste_models[index])



        if(j!=0):
            axs[i,j].set_yticklabels([])
        else:
            axs[i,j].set_ylabel('°C')
        if(i!=2):
            axs[i,j].set_xticklabels([])
        else:
            axs[i,j].set_xlabel('Years')
            axs[i,j].set_xticks([0, 20,  40,  60,  80,  100, 114])
            axs[i,j].set_xticklabels(
                ['1900',  '1920',  '1940',  '1960',  '1980', '2000', '2014'])
        axs[0,0].legend(frameon=False)
        



    plt.savefig('figures/CI_data')
    plt.show()

#fig_CI()


def get_data_set_for_tls(model='ALL',cluster=-1,filtrage=False,mean=True):
    
    liste_models = ['CanESM5', 'CNRM', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
                    'NorESM2','CESM2','GISS']

    aer = []
    nat = []
    historical = []
    ghg = []
    noise = np.empty((1,115))
    
    

    count = np.array([0.,0.,0.,0.])

    for model_curr in liste_models:
        if(model_curr!=model):
            print(model_curr)
    
            aer_curr = get_data_forcage('hist-aer', model=model_curr,cluster=cluster,filtrage=filtrage)[:, 0:115]
            nat_curr = get_data_forcage('hist-nat', model=model_curr,cluster=cluster,filtrage=filtrage)[:, 0:115]
            historical_curr = get_data_forcage('historical', model=model_curr,cluster=cluster,filtrage=filtrage)[:, 0:115]
            ghg_curr = get_data_forcage('hist-GHG', model=model_curr,cluster=cluster,filtrage=filtrage)[:, 0:115]
            
            
            aer_bruit = (aer_curr - np.mean(aer_curr,axis=0)) * np.sqrt((aer_curr.shape[0])/(aer_curr.shape[0]-1))
            noise = np.concatenate((noise,aer_bruit[0:-1]))
            
            ghg_bruit = (ghg_curr - np.mean(ghg_curr,axis=0)) * np.sqrt((ghg_curr.shape[0])/(ghg_curr.shape[0]-1))
            noise = np.concatenate((noise,ghg_bruit[0:-1]))
            
            nat_bruit = (nat_curr - np.mean(nat_curr,axis=0)) * np.sqrt((nat_curr.shape[0])/(nat_curr.shape[0]-1))
            noise = np.concatenate((noise,nat_bruit[0:-1]))
            hist_bruit = (historical_curr - np.mean(historical_curr,axis=0)) * np.sqrt((historical_curr.shape[0])/(historical_curr.shape[0]-1))
            noise = np.concatenate((noise,hist_bruit[0:-1]))
            
    
            
            
            count+= np.array([1/ghg_curr.shape[0],1/aer_curr.shape[0],1/nat_curr.shape[0],1/historical_curr.shape[0]])
            
    
            
            aer.append(np.mean(aer_curr,axis=0))
            ghg.append(np.mean(ghg_curr,axis=0))
            nat.append(np.mean(nat_curr,axis=0))
            historical.append(np.mean(historical_curr,axis=0))

    noise = noise[1:]
    number_model = np.array(aer).shape[0]
    print(number_model)
    if(mean):
        aer = np.mean(np.array(aer),axis=0)
        nat = np.mean(np.array(nat),axis=0)
        historical = np.mean(np.array(historical),axis=0)
        ghg = np.mean(np.array(ghg),axis=0)
    mean = np.array([ghg,aer,nat,historical])
    
    count = (number_model * number_model) / count


    return mean, count, noise

    
def get_mean_all():
    
    liste_models = ['CanESM5', 'CNRM', 'IPSL', 'ACCESS', 'BCC', 'FGOALS', 'HadGEM3', 'MIRO', 'ESM2',
                    'NorESM2','CESM2','GISS']
    result = []
    historical = []
    for model_curr in liste_models :
    
        aer_ipsl = np.mean(get_data_forcage('hist-aer',model=model_curr),axis=0)
        ghg_ipsl = np.mean(get_data_forcage('hist-GHG',model=model_curr),axis=0)
        nat_ipsl = np.mean(get_data_forcage('hist-nat',model=model_curr),axis=0)
        historical_ipsl = np.mean(get_data_forcage('historical',model=model_curr),axis=0)
    
    
    
    
        result_ipsl = np.stack((ghg_ipsl, aer_ipsl, nat_ipsl))
        result.append(result_ipsl)
        historical.append(historical_ipsl)
    
    result = np.array(result)
    
    
    historical = np.array(historical)
    
    
    return result, historical














#
#plot_mean_simus()
